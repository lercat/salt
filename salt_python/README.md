[![pipeline status](https://gitlab.windenergy.dtu.dk/lercat/salt/badges/main/pipeline.svg)](https://gitlab.windenergy.dtu.dk/lercat/salt/commits/main)
[![coverage report](https://gitlab.windenergy.dtu.dk/lercat/salt/badges/main/coverage.svg)](https://gitlab.windenergy.dtu.dk/lercat/salt/commits/main)

# SALT - Python

The Python package of SALT. Install by performing:

`pip install -e .`

Below you find a translation of the SALT class docstring (credit ChatGPT) and have a look at the jupyter notebook `salt/example.ipynb` to get started.

## SALT Python Class

A Python class for computing Annual Energy Production (AEP) loss from leading edge roughness. This implementation is based on the model presented in:

Christian Bak 2022 J. Phys.: Conf. Ser. 2265 032038

Equation numbers are provided when applicable. Outputs are presented as attributes, and the class needs to be called first to obtain them.

### Inputs & Attributes

- `rated_power` (float [W]): Wind turbine rated power.
- `radius` (float [m]): Wind turbine rotor radius.
- `site` (object): Site object.
- `cutin_ws` (float [m/s]): Turbine cut-in wind speed, used with `cutout_ws` and `dws` to determine wind speed bin centers. Alternatively, wind speeds can be directly set through `ws`. (default: 3.)
- `cutout_ws` (float [m/s]): Turbine cut-out wind speed. (default: 25.)
- `dws` (float [m/s]): Wind speed bin size. (default: 1.)
- `ws` (1D-array(float) [m/s]): Wind speed bin centers, overwrites the above. (default: None)
- `r_clcd` (2D-array(float)): Radial distribution of lift-to-drag ratio. Columns are r/R, cl/cd. Default values are provided. (default: np.array([[.29, 6.], [.3, 78.], [.69, 78.], [.7, 110.]]))
- `name` (string): Name of the run. (default: SALT)
- `max_tip_speed` (float [m/s]): Turbine maximum tip speed. (default: 80.)
- `optimum_tsr` (float): Turbine optimum tip-speed-ratio. (default: 9.)
- `drivetrain_efficiency` (float): Turbine drivetrain efficiency. A default value is given if not available. (default: 0.94)
- `n_blades` (int): Number of turbine blades, usually three. (default: 3)
- `ct_inv` (float): Inviscid/optimal thrust coefficient. (default: 8. / 9.)
- `cp_opt` (float): Optimal power coefficient. (default: 16. / 27.)
- `cl_design` (float): Design lift coefficient. (default: 1.13)
- `ct2a` (method): Conversion from CT to induction factor. (default: ct2a_mom1d)

### Outputs & Attributes

- `<input_name>`: All inputs are stored.
- `air_density` (1D-array [kg/m^3]): Air density taken from the site.
- `ws` (1D-array(float) [m/s]): Wind speed bin centers.
- `ws_edges` (1D-array(float), size=Nws [m/s]): Wind speed bin edges.
- `ws_edges` (1D-array(float), size=Nws+1 [m/s]): Wind speed bin edges.
- `tsr` (1D-array(float), size=Nws): Tip-speed-ratio.
- `ct` (1D-array(float), size=Nws): Turbine thrust coefficient.
- `rotor_area` (float [m^2]): Turbine rotor area.
- `specific_rated_power` (float [W/m^2]): Specific rated power.
- `r` (1D-array(float), size=Nr): Blade section edge positions.
- `rc` (1D-array(float), size=Nr-1): Blade section center positions.
- `rc_clcd_clean` (1D-array, size=Nr-1): Radial lift-to-drag ratio distribution of clean blades.
- `rc_clcd_clrat_ler` (3D-array, size=(Nr-1,2,Nblades)): Radial lift-to-drag ratio distribution of LER blades.
- `rc_dcp_loss` (2D-array, size=(Nr-1,Nblades,Nws)): Loss in local Cp along the blade for each wind speed.
- `cp_clean` (1D-array, size=Nws): Clean rotor CP variation with wind speed.
- `cp_ler` (1D-array, size=Nws): LER rotor CP variation with wind speed.
- `rel_cp_loss` (1D-array, size=Nws): Relative change in rotor CP from LER (not in percentage).
- `power_clean` (1D-array, size=Nws): Clean rotor power production variation with wind speed.
- `power_ler` (1D-array, size=Nws): LER rotor power production variation with wind speed.
- `aep_clean` (float): Annual energy production of the clean rotor.
- `aep_ler` (float): Annual energy production of the LER rotor.
- `rel_aep_loss` (float): Relative change in AEP from LER (not in percentage).
- `ct_clean` (1D-array, size=Nws): Clean rotor thrust coefficient, CT, variation with wind speed.
- `ct_ler` (1D-array, size=Nws): LER rotor thrust coefficient, CT, variation with wind speed.
- `rel_ct_loss` (1D-array, size=Nws): Relative change in rotor CT from LER (not in percentage).

### Methods

- `__call__(r_fclcd_ler)`: Invoke calculation of AEP loss. `r_fclcd_ler` is a 2D-array holding the definition of the blade damages.
- `plot_clcd(self, r=np.linspace(0., 1., 100), axs=None, lw=1.5, ms=4)`: Plot lift-to-drag variation for clean and LER rotors.
- `plot_tsr(self, axs=None, lw=1.5, ms=4)`: Plot tip-speed-ratio curve.
- `plot_cp(self, axs=None, lw=1.5, ms=2)`: Plot variation of power coefficient for clean and LER rotors.
- `plot_power(self, axs=None, lw=1, ms=2)`: Plot variation of power for clean and LER rotors.
- `plot_power_loss(self, axs=None, lw=1, ms=2)`: Plot variation of relative power loss from LER.
- `plot_ct(self, axs=None, lw=1.5, ms=2)`: Plot variation of thrust coefficient for clean and LER rotors.

