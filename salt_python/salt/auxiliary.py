#!/usr/bin/env python3
"""
Script containing auxilary classes

Author: Alexander Meyer Forsting (alrf@dtu.dk)
Created on: 25-01-2024
Copyright (c) 2024 DTU Wind & Energy Systems
License: MIT
"""
import numpy as np
from numpy import newaxis as na
from scipy.interpolate import PchipInterpolator
import matplotlib.pylab as plt


class pchip():
    """
    Pchip interpolator class based on the scipy equivalent, however if
    interpolation is interpolated outside the input value range, the
    closest value is taken.
    """
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.interp = PchipInterpolator(x, y)

    def __call__(self, xi):
        yi = self.interp(xi)
        yi[xi < self.x[0]] = self.y[0]
        yi[xi > self.x[-1]] = self.y[-1]

        return yi


def ct2a_madsen(ct, ct2ap=np.array([0.2460, 0.0586, 0.0883])):
    """
    BEM axial induction approximation by
    Madsen, H. A., Larsen, T. J., Pirrung, G. R., Li, A., and Zahle, F.: Implementation of the blade element momentum model on a polar grid and its aeroelastic load impact, Wind Energ. Sci., 5, 1–27, https://doi.org/10.5194/wes-5-1-2020, 2020.
    """
    # Evaluate with Horner's rule.
    # ct2a_ilk = ct2ap[2] * ct_ilk**3 + ct2ap[1] * ct_ilk**2 + ct2ap[0] * ct_ilk
    return ct * (ct2ap[0] + ct * (ct2ap[1] + ct * ct2ap[2]))


def ct2a_mom1d(ct):
    """
    1D momentum, CT = 4a(1-a), with CT forced to below 1.
    """
    return 0.5 * (1. - np.sqrt(1. - np.minimum(1, ct)))


class UniformWeibullSite():
    """
    Class defining the site at which the turbine is located need in
    SALT(), using a Weibull distribution.


    Inputs & Attributes
    =============

    weibull_k : float
        Weibull shape parameter, for a Rayleigh distribution k=2

    weibull_A : float
        Weibull scale parameter, usually close to rated wind speed


    Methods
    =============

    __call__(ws)
        get Weibull weights at wind speeds, ws

    int_weight(ws)
        integrate Weibull weights from 0 to wind speed, ws

    int_weight_range(ws0, ws1)
        integrate Weibull weights from wind speed ws0 to ws1

    plot_weibull(ws=np.linspace(0., 30., 100), axs=None)
        plot the weibull distribution at wind speeds, ws

    """

    def __init__(self, weibull_k, weibull_A, air_density=1.225):
        self.k = weibull_k
        self.A = weibull_A
        self.air_density = air_density

    def __call__(self, ws):
        return (self.k / self.A) * (ws / self.A)**(self.k - 1.) * np.exp(-(ws / self.A)**self.k)

    def int_weight(self, ws):
        return (1. - np.exp(-(ws / self.A)**self.k))

    def int_weight_range(self, ws0, ws1):
        return self.int_weight(ws1) - self.int_weight(ws0)

    def plot_weibull(self, ws=np.linspace(0., 30., 100), axs=None):
        w = self.__call__(ws)
        wint = self.int_weight(ws)
        if axs is None:
            fig, axs = plt.subplots(1, 1)
        axs.set_title('Weibull distribution: k={:.1f}, A={:.1f}m/s'.format(self.k, self.A))
        # weights
        axs.set_xlabel('V [m/s]')
        color = 'tab:red'
        axs.plot(ws, w, '-', color=color)
        axs.set_ylabel(r'$w$ [-]', color=color)
        axs.tick_params(axis='y', labelcolor=color)
        # integral weights
        axs2 = axs.twinx()
        color = 'tab:blue'
        axs2.plot(ws, wint, '-', color=color)
        axs2.set_ylabel(r'$\int w$ [-]', color=color)
        axs2.tick_params(axis='y', labelcolor=color)

        return axs, fig