#!/usr/bin/env python3
"""
Main script defining the SALT class

Author: Alexander Meyer Forsting (alrf@dtu.dk)
Created on: 25-01-2024
Copyright (c) 2024 DTU Wind & Energy Systems
License: MIT
"""
import numpy as np
from numpy import newaxis as na
import matplotlib.pylab as plt

from salt.auxiliary import pchip, UniformWeibullSite, ct2a_mom1d

colors, lv = np.zeros((3, 3)), 0.9
colors[0, :] = plt.cm.Blues(lv)[:3]
colors[1, :] = plt.cm.Reds(lv)[:3]
colors[2, :] = plt.cm.Greens(lv)[:3]

class SALT():
    """
    A python class for computing AEP loss from leading edge roughness. This is an
    implementation of the model presented in:
    Christian Bak 2022 J. Phys.: Conf. Ser. 2265 032038
    Equation numbers are given if applicable. Outputs are given in the form of
    attributes, you need to call the class first to get them.

    Inputs & Attributes
    =============

    rated_power : float [W]
        wind turbine rated power

    radius : float [m]
        wind turbine rotor radius

    site : object
        site object

    cutin_ws : float [m/s]
        turbine cut-in wind speed, used together with cutout_ws and dws to
        determine wind speed bin centres. Alernatively the wind speeds can
        be dirctly set through ws.

        (default: 3.)

    cutout_ws : float [m/s]
        turbine cut-out wind speed

        (default: 25.)

    dws : float [m/s]
        wind speed bin size

        (default: 1.)

    ws : 1D-array(float) [m/s]
        wind speed bin centres, overwrites the above

        (default: None)

    r_clcd : 2D-array(float)
        radial distribution of lift-to-drag ratio.
        Columns are r/R, cl/cd, note that the radial location
        is normalised by the radius and if not the entire range in r is defined
        from 0 to 1, then it is assumed that the end values apply.

        (default: np.array([[.29, 6.], [.3, 78.], [.69, 78.], [.7, 110.]]))

    name : string
        name of run

        (default: SALT)

    max_tip_speed : float [m/s]
        turbine maximum tip speed

        (default: 80.)

    optimum_tsr : float
        turbine optimum tip-speed-ratio

        (default: 9.)

    drivetrain_efficiency : float
        turbine drive train efficiency, a default value is given if not
        available

        (default: 0.94)

    n_blades : int
        number of turbine blades, usually three

        (default: 3)

    ct_inv : float
        inviscid/optimal thrust coefficient

        (default: 8. / 9.)

    cp_opt : float
        optimal power coefficient

        (default: 16. / 27.)

    cl_design : float
        design lift coefficient

        (default: 1.13)

    ct2a : method
        conversion from CT to induction factor

        (default: ct2a_mom1d)


    Outputs & Attributes
    =============

    <input_name> :
        all inputs are stored

    air_density : 1D-array [kg/m^3]
        air density taken from site

    ws : 1D-array(float) [m/s]
        wind speed bin centres

    ws_edges : 1D-array(float), size=Nws [m/s]
        wind speed bin edges

    ws_edges : 1D-array(float), size=Nws+1 [m/s]
        wind speed bin edges

    tsr : 1D-array(float), size=Nws
        tip-speed-ratio

    ct : 1D-array(float), size=Nws
        turbine thrust coefficient

    rotor_area : float [m^2]
        turbine rotor area

    specific_rated_power: float [W/m^2]
        specific rated power

    specific_rated_power: float [W/m^2]
        specific rated power

    r: 1D-array(float), size=Nr
        blade section edge positions

    rc: 1D-array(float), size=Nr-1
        blade section centre positions

    rc_clcd_clean: 1D-array, size=Nr-1
        radial lift-to-drag ratio distribution of clean blades,
        only given for one as blades are identical

    rc_clcd_clrat_ler: 3D-array, size=(Nr-1,2,Nblades)
        radial lift-to-drag ratio distribution (:,0,:) and
        radial lift ratio distribution (:,1,:) of LER blades

    rc_dcp_loss: 2D-array, size=(Nr-1,Nblades,Nws)
        loss in local Cp along blade for each wind speed

    cp_clean: 1D-array, size=Nws
        clean rotor CP variation with wind speed

    cp_ler: 1D-array, size=Nws
        LER rotor CP variation with wind speed

    rel_cp_loss: 1D-array, size=Nws
        relative change in rotor CP from LER (not in percentage)

    power_clean: 1D-array, size=Nws
        clean rotor power production variation with wind speed

    power_ler: 1D-array, size=Nws
        LER rotor power production variation with wind speed

    aep_clean: float
        annual energy production of clean rotor

    aep_ler: float
        annual energy production of LER rotor

    rel_aep_loss: float
        relative change in AEP from LER (not in percentage)

    ct_clean: 1D-array, size=Nws
        clean rotor thrust coefficient, CT, variation with wind speed

    ct_ler: 1D-array, size=Nws
        LER rotor thrust coefficient, CT, variation with wind speed

    rel_ct_loss: 1D-array, size=Nws
        relative change in rotor CT from LER (not in percentage)


    Methods
    =============

    __call__(r_fclcd_ler)
        invoke calculation of AEP loss
        dclcd_ler is a 2D-array holding the definition of the blade damages.
        The expected input is an array with four columns and N+1 rows, where N is the number
        of blade sections:

        0. Normalised radial position of blade section edges, $r/R$
        1. L/D fraction per section for blade #0
        2. L/D fraction per section for blade #1
        3. L/D fraction per section for blade #2

    plot_clcd(self, r=np.linspace(0., 1., 100), axs=None, lw=1.5, ms=4)
        plot lift-to-drag variation for clean and LER rotors

    plot_tsr(self, axs=None, lw=1.5, ms=4)
        plot tip-speed-ratio curve

    plot_cp(self, axs=None, lw=1.5, ms=2)
        plot variation of power cofficient for clean and LER rotors

    plot_power(self, axs=None, lw=1, ms=2)
        plot variation of power for clean and LER rotors

    plot_power_loss(self, axs=None, lw=1, ms=2)
        plot variation of relative power loss from LER

    plot_ct(self, axs=None, lw=1.5, ms=2)
        plot variation of thrust cofficient for clean and LER rotors

    """
    def __init__(self, rated_power, radius, site,
                 cutin_ws=3., cutout_ws=25., dws=1.0, ws=None,
                 r_clcd=np.array([[.29, 6.], [.3, 78.], [.69, 78.], [.7, 110.]]),
                 name='SALT', max_tip_speed=80., optimum_tsr=9., drivetrain_eff=0.94, n_blades=3,
                 ct_inv=8. / 9., cp_opt=16. / 27., cl_design=1.13,
                 ct2a=ct2a_mom1d):

        self.rated_power = rated_power
        self.radius = radius
        self.site = site
        self.air_density = site.air_density

        if ws is not None:
            self.cutin_ws = np.min(ws)
            self.cutout_ws = np.max(ws)
            self.ws = ws
        else:
            self.cutin_ws = cutin_ws
            self.cutout_ws = cutout_ws
            self.ws = np.arange(cutin_ws, cutout_ws + dws, dws)
        ws_edges = np.ones(len(self.ws) + 1)
        ws_edges[1:-1] = (self.ws[1:] + self.ws[:-1]) / 2.
        ws_edges[0] = self.ws[0] - np.diff(self.ws)[0] / 2.
        ws_edges[-1] = self.ws[-1] + np.diff(self.ws)[-1] / 2.
        self.ws_edges = ws_edges

        self.r_clcd = r_clcd
        self.name = name
        self.max_tip_speed = max_tip_speed
        self.optimum_tsr = optimum_tsr
        self.drivetrain_eff = drivetrain_eff
        self.n_blades = n_blades

        self.ct_inv = ct_inv
        self.cp_opt = cp_opt
        self.cl_design = cl_design

        self.ct2a = ct2a

        # build interpolating splines
        self._interp_clcd = pchip(self.r_clcd[:, 0], self.r_clcd[:, 1])

        # calculate tip-speed-ratio
        self.calc_tsr()

        # calculate thrust coefficient
        self.calc_ct_opt()

        # specific power
        self.rotor_area = (np.pi * self.radius**2)
        self.specific_rated_power = self.rated_power / self.rotor_area

    def calc_tsr(self):
        # actual tip-speed-ratio is limited by max. tip speed
        tsr = np.ones_like(self.ws) * self.optimum_tsr
        tip_speed = self.optimum_tsr * self.ws
        ii = tip_speed > self.max_tip_speed
        tsr[ii] = self.max_tip_speed / self.ws[ii]
        self.tsr = tsr

    def calc_ct_opt(self):
        # calculate drop in CT from drop in tsr modified Eq. (17)
        ctp = (6.8e-3 * self.tsr**3 - 0.203 * self.tsr**2 + 2.08 * self.tsr - 7.71)
        dct = self.tsr**2 * 2 * np.pi * np.diff(self.tsr, prepend=self.tsr[0]) * ctp / (180. / np.pi) * 0.0675 / (np.pi * self.cl_design)
        # thrust coefficient including losses
        ct = np.ones_like(self.ws) * self.ct_inv
        ct -= np.cumsum(dct)
        self.ct_opt = ct

    def clcd(self, r):
        return self._interp_clcd(r)

    def calc_dcp(self, r, tsr, clrat, clcd, ct):
        return self._calc_cp(r[1:], tsr, clrat, clcd, ct) - self._calc_cp(r[:-1], tsr, clrat, clcd, ct)


    def _calc_cp(self, r, tsr, clrat, clcd, ct):
        # calculate loss in Cp
        a_inv = self.ct2a(ct)[na, na, :]
        a_vis = self.ct2a(clrat[:, :, na] * ct[na, na, :])
        # Eq. (15)
        t0 = (np.pi * r**2)[:, na, na]
        t1 = clrat[:, :, na] * (np.pi * r[:, na, na]**2 * (1. - a_vis) - ((tsr[na, na, :] * 2. / 3. * np.pi * r[:, na, na]**3) / clcd[:, :, na])) / (1. - a_inv)
        t2 = 2. / (tsr[na, na, :] * self.n_blades + 2.) * np.pi * r[:, na, na]**(tsr[na, na, :] * self.n_blades + 2.)
        t3 = clrat[:, :, na] * (t2 * (1. - a_vis) - ((tsr[na, na, :] * (2. / (tsr[na, na, :] * self.n_blades + 3.)) * np.pi * r[:, na, na]**((tsr[na, na, :] * self.n_blades + 3.))) / clcd[:, :, na])) / (1. - a_inv)

        return 1. / np.pi * (t0 - t1 - (t2 - t3))

    def calc_power(self, cp):
        return 0.5 * self.air_density * self.ws**3. * cp * self.rotor_area

    def calc_aep(self, power):
        weights = (self.site.int_weight(self.ws_edges[1:]) - self.site.int_weight(self.ws_edges[:-1]))
        return (24 * 365 * weights * power).sum()

    def calc_ct(self, rc, r, tsr, clrat, clcd, ct):
        a_vis = self.ct2a(clrat[:, :, na] * ct[na, na, :])
        return np.pi * np.diff(r**2)[:, na, na] * 4. * a_vis * (1. - a_vis) * (1. - rc[:, na, na]**(tsr[na, na, :] * self.n_blades))

    def approx_ct(self, cp, c=[20.553, -21.23, 7.3631, 0.2783, 0.0003]):
        x = cp / np.max(cp) * self.cp_opt
        return c[0] * x**4 + c[1] * x**3 + c[2] * x**2 + c[3] * x + c[4]

    def calc_thrust(self, ct):
        return 0.5 * self.air_density * self.ws**2. * ct * self.rotor_area

    def __call__(self, r_fclcd_ler):

        # ---aerodynamic losses
        # store input
        self.r_fclcd_ler = r_fclcd_ler
        # calculate LER performance distributions
        r = r_fclcd_ler[:, 0]
        self.r = r
        self.rc = (r[1:] + r[:-1]) / 2.
        self.rc_clcd_clean = self.clcd(self.rc)
        rc_clcd_ler = r_fclcd_ler[:-1, 1:] * self.rc_clcd_clean[:, na]
        # empirical correlation derived from wind tunnel data to estimate loss in Cl
        rc_clrat_ler = r_fclcd_ler[:-1, 1:]**(1. / 3.)
        self.rc_clcd_clrat_ler = np.stack((rc_clcd_ler, rc_clrat_ler), axis=1)

        # ---power coefficient viscous losses
        # compute sectionwise viscous losses for clean case (only obe blade)
        rc_dcp_clean = self.calc_dcp(self.r, self.tsr, np.ones_like(self.rc)[:, na], self.rc_clcd_clean[:, na], self.ct_opt)
        # not devided by three as only one
        dcp_clean = np.sum(rc_dcp_clean[:, 0, :], axis=(0))
        # compute with leading edge roughness
        rc_dcp_ler = self.calc_dcp(self.r, self.tsr, rc_clrat_ler, rc_clcd_ler, self.ct_opt)
        dcp_ler = np.sum(np.sum(rc_dcp_ler, axis=0) / self.n_blades, axis=0)
        # total loss per damage is difference between viscouss losses with LER and without (loss is positive here)
        # Eq. (18)
        self.rc_dcp_loss = rc_dcp_ler - rc_dcp_clean

        # ---power coefficients
        # correct optimum performance for off-design conditions
        # compute difference between optimum and actual Cp
        a_inv = self.ct2a(self.ct_opt)
        dcp_optact = (self.cp_opt - 4. * a_inv * ((1. - a_inv)**2)) * (1. - (2. / (self.optimum_tsr * self.n_blades + 2.)))
        # clean rotor Cp
        cp_clean = (self.cp_opt * (1. - (2. / (self.optimum_tsr * self.n_blades + 2))) * (1. - dcp_clean) - dcp_optact) * self.drivetrain_eff
        # specific Cp
        cp_specific = self.specific_rated_power / (0.5 * self.air_density * self.ws**3)
        # use specific Cp to limit the computed Cp
        self.cp_clean = np.minimum(cp_clean, cp_specific)
        # LER rotor Cp
        cp_ler = (self.cp_opt * (1. - (2. / (self.optimum_tsr * self.n_blades + 2))) * (1. - dcp_ler) - dcp_optact) * self.drivetrain_eff
        # limit
        self.cp_ler = np.minimum(np.minimum(cp_ler, cp_specific), self.cp_clean)
        # calculate relative change
        self.rel_cp_loss = 1. - self.cp_ler / self.cp_clean

        # ---power curves
        self.power_clean = self.calc_power(self.cp_clean)
        self.power_ler = self.calc_power(self.cp_ler)

        # ---AEP
        self.aep_clean = self.calc_aep(self.power_clean)
        self.aep_ler = self.calc_aep(self.power_ler)
        # relative loss in aep
        self.rel_aep_loss = 1. - self.aep_ler / self.aep_clean

        # ---thrust coefficient losses
        # approximation of ct from cp used to limit ct
        ct_approx = self.approx_ct(self.cp_clean)
        # addition with respect to the original paper
        rc_ct_clean = self.calc_ct(self.rc, self.r, self.tsr, np.ones_like(self.rc)[:, na], self.rc_clcd_clean[:, na], self.ct_opt)
        ct_clean = np.sum(rc_ct_clean[:, 0, :], axis=(0)) / np.pi
        self.ct_clean = np.minimum(ct_clean, ct_approx)
        # compute with leading edge roughness
        rc_ct_ler = self.calc_ct(self.rc, self.r, self.tsr, rc_clrat_ler, rc_clcd_ler, self.ct_opt)
        ct_ler = np.sum(np.sum(rc_ct_ler, axis=0), axis=0) / (np.pi * self.n_blades)
        self.ct_ler = np.minimum(ct_ler, ct_approx)
        # calculate relative change
        self.rel_ct_loss = 1. - self.ct_ler / self.ct_clean

    def plot_clcd(self, r=np.linspace(0., 1., 100), axs=None, lw=1.5, ms=4):
        if axs is None:
            fig, axs = plt.subplots(1, 1)
        axs.set_title('{}: {}'.format(self.name, 'L/D distributions'))
        axs.plot(r, self.clcd(r), 'k--', alpha=0.5, lw=lw, ms=ms, label='clean input')
        axs.stairs(self.rc_clcd_clean, ls=':', edges=self.r, lw=lw, color=colors[0], label='clean sections')
        for i in range(self.n_blades):
            axs.stairs(self.rc_clcd_clrat_ler[:, 0, i], alpha=0.8, edges=self.r, lw=lw, label='LER blade {}'.format(i))
        axs.legend()
        axs.set_xlabel(r'Normalised radius, $r/R$ [-]')
        axs.set_ylabel(r'Lift-to-drag ratio, $L/D$ [-]')
        return axs, fig

    def plot_clrat(self, r=np.linspace(0., 1., 100), axs=None, lw=1.5, ms=4):
        if axs is None:
            fig, axs = plt.subplots(1, 1)
        axs.set_title('{}: {}'.format(self.name, 'L/D distributions'))
        axs.plot(r, np.ones_like(r), 'k--', alpha=0.5, lw=lw, ms=ms, label='clean')
        for i in range(self.n_blades):
            axs.stairs(self.rc_clcd_clrat_ler[:, 1, i], alpha=0.8, edges=self.r, lw=lw, label='LER blade {}'.format(i))
        axs.legend()
        axs.set_xlabel(r'Normalised radius, $r/R$ [-]')
        axs.set_ylabel(r'LER lift factor [-]')
        return axs, fig

    def plot_tsr(self, axs=None, lw=1.5, ms=4):
        if axs is None:
            fig, axs = plt.subplots(1, 1)
        axs.set_title('{}: {}'.format(self.name, 'Tip-speed-ratio'))
        axs.plot(self.ws, self.tsr, '.-', lw=lw, ms=ms, color=colors[0])
        axs.set_xlabel(r'Wind speed, [m/s]')
        axs.set_ylabel(r'Tip-speed-ratio, $\lambda$ [-]')
        return axs, fig

    def plot_cp(self, axs=None, lw=1.5, ms=2):
        if axs is None:
            fig, axs = plt.subplots(1, 1)
        axs.set_title('{}: {}'.format(self.name, 'Power coefficient'))
        axs.plot(self.ws, self.cp_clean, 's-', label='clean', lw=lw, ms=ms, color=colors[0])
        axs.plot(self.ws, self.cp_ler, 'o-', label='LER', lw=lw, ms=ms, color=colors[1])
        axs.legend()
        axs.set_xlabel(r'V [m/s]')
        axs.set_ylabel(r'$C_P$ [-]')
        return axs, fig

    def plot_power(self, axs=None, lw=1, ms=2):
        if axs is None:
            fig, axs = plt.subplots(1, 1)
        axs.set_title('{}: {}'.format(self.name, 'Power production'))
        axs.plot(self.ws, self.power_clean / 1e6, 's-', label='clean', lw=lw, ms=ms, color=colors[0])
        axs.plot(self.ws, self.power_ler / 1e6, 'o-', label='LER', lw=lw, ms=ms, color=colors[1])
        axs.legend()
        axs.set_xlabel(r'V [m/s]')
        axs.set_ylabel(r'Power [MW]')
        return axs, fig

    def plot_power_loss(self, axs=None, lw=1, ms=2):
        if axs is None:
            fig, axs = plt.subplots(1, 1)
        axs.set_title('{}: {}'.format(self.name, 'Power loss'))
        axs.plot(self.ws, (1. - self.power_ler / self.power_clean) * 1e2, 'o-', label='clean', lw=lw, ms=ms, color=colors[0])
        axs.set_xlabel(r'V [m/s]')
        axs.set_ylabel(r'Power loss [%]')
        return axs, fig

    def plot_ct(self, axs=None, lw=1.5, ms=2):
        if axs is None:
            fig, axs = plt.subplots(1, 1)
        axs.set_title('{}: {}'.format(self.name, 'Thrust coefficient'))
        axs.plot(self.ws, self.ct_clean, 's-', label='clean', lw=lw, ms=ms, color=colors[0])
        axs.plot(self.ws, self.ct_ler, 'o-', label='LER', lw=lw, ms=ms, color=colors[1])
        axs.legend()
        axs.set_xlabel(r'V [m/s]')
        axs.set_ylabel(r'$C_T$ [-]')
        return axs, fig


def main():
    if __name__ == '__main__':

        # this is a copy of demo.py
        # for explanatory text refer to that notebook
        import pandas as pd

        # columns: r/R | clcd | clrat
        r_clcd = np.array([[.29, 6.],
                           [.3, 78.],
                           [.69, 78.],
                           [.7, 110.]])

        weibull_k = 2.       # [-]
        weibull_A = 8.       # [m/s]
        air_density = 1.225  # [kg/m^3]
        site = UniformWeibullSite(weibull_k, weibull_A, air_density=air_density)

        rated_power = 2000e3  # [W]
        radius = 45.          # [m]

        loss = SALT(rated_power, radius, site,
                    cutin_ws=3., cutout_ws=25., dws=1.0,
                    r_clcd=r_clcd, name='SALT Example',
                    max_tip_speed=80., optimum_tsr=9., drivetrain_eff=0.94, n_blades=3,
                    ct_inv=8. / 9., cp_opt=16. / 27., cl_design=1.13,
                    ct2a=ct2a_mom1d)

        # read the csv file into a DataFrame
        df = pd.read_csv('damage_input.csv', header=[0, 1])

        categories = {'a': 1.0, 'b': 0.9, 'c': 0.7, 'd': 0.5, 'e': 0.3}
        df.replace(categories, inplace=True)
        r_fclcd_ler = df.iloc[:, [0, 2, 3, 4]].to_numpy()

        loss(r_fclcd_ler)
        print('AEP loss: {:.2f} %'.format(loss.rel_aep_loss * 1e2))

        loss.plot_power_loss()
        loss.plot_clcd()
        loss.plot_tsr()
        loss.plot_cp()
        loss.plot_ct()
        loss.plot_power()
        plt.show()


main()
