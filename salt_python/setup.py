
from setuptools import setup, find_packages

kwargs = {'author': 'A Meyer Forsting',
 'author_email': 'alrf@dtu.dk',
 'description': '',
 'download_url': '',
 'include_package_data': True,
 'install_requires': ['numpy', 'scipy', 'matplotlib', 'pandas'],
 'keywords': [],
 'license': '',
 'maintainer': '',
 'maintainer_email': '',
 'name': 'SALT',
 'packages': ['salt'],
 'version': '0.1',
 'zip_safe': False}


setup(**kwargs)
