import numpy as np
import os
import numpy.testing as npt
import pytest
import sys
import pandas as pd

from salt.main import SALT
from salt.auxiliary import UniformWeibullSite, ct2a_mom1d

#@pytest.fixture
def test_aep():

    # columns: r/R | clcd | clrat
    r_clcd = np.array([[.29, 6.],
                       [.3, 78.],
                       [.69, 78.],
                       [.7, 110.]])

    weibull_k = 2.       # [-]
    weibull_A = 8.       # [m/s]
    air_density = 1.225  # [kg/m^3]
    site = UniformWeibullSite(weibull_k, weibull_A, air_density=air_density)

    rated_power = 2000e3  # [W]
    radius = 45.          # [m]

    loss = SALT(rated_power, radius, site,
                cutin_ws=3., cutout_ws=25., dws=1.0,
                r_clcd=r_clcd, name='SALT Example',
                max_tip_speed=80., optimum_tsr=9., drivetrain_eff=0.94, n_blades=3,
                ct_inv=8. / 9., cp_opt=16. / 27., cl_design=1.13,
                ct2a=ct2a_mom1d)

    # read the csv file into a DataFrame
    df = pd.read_csv('../salt/damage_input.csv', header=[0, 1])

    categories = {'a': 1.0, 'b': 0.9, 'c': 0.7, 'd': 0.5, 'e': 0.3}
    df.replace(categories, inplace=True)
    r_fclcd_ler = df.iloc[:, [0, 2, 3, 4]].to_numpy()

    loss(r_fclcd_ler)
    npt.assert_almost_equal(0.020815442581428645, loss.rel_aep_loss, 5)

    return
