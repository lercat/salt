# SALT - Simplified Aerodynamic Loss Tool

A fast BEM-based tool to predict the loss in annual energy production for a wind turbine, due to aerodynamic deterioration of different spanwise sections of its blades. It relies on a simplified BEM model to compute the aerodynamic performance of the rotor and perturbs the sectional lift coefficient and lift-to-drag ratio to assess the losses.

#### One tool - two versions

The tool is available as Excel spreadsheet [here](Excel/) and as a Python package [here](Python/).
Usage differs between versions, thus version-specific documentation is contained within the READMEs inside each respective folder.

## Introduction

This calculation tool is initially made to predict the annual aerodynamic energy loss relative to the starting point.
The motivation to formulate this model is that wind turbine owners neither have much information about the wind turbine nor information about the real surface conditions of the blades - apart from photos from inspections.
Therefore, this tool basically only requires a few parameters: Rated power, rotor radius, air density, Weibull parameters A and k and categories describing the surface conditions of each blade. The remaining parameters required to describe the rotor operation is assumed.

The tool is based on the work described in: [Christian Bak 2022 J. Phys.: Conf. Ser. 2265 032038](https://iopscience.iop.org/article/10.1088/1742-6596/2265/3/032038/pdf)

The tool should be able to predict the energy loss within 10% accuracy according to initial investigations, but further validation of the tool will be carried out.
Also, the tool is not made to predict precise absolute annual energy production. It is made for relative changes.

## Authors

Christian Bak (chba@dtu.dk) and Alexander Meyer Forsting (alrf@dtu.dk)

## License

[MIT](LICENSE)

## How to cite

Christian Bak, & Meyer Forsting, A. R. (2023). SALT - Simplified Aerodynamic Loss Tool (1.0.0 - beta). DTU Wind, Technical University of Denmark. https://doi.org/10.5281/zenodo.7906333
