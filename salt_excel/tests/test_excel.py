import unittest
import filecmp

class ExcelTests(unittest.TestCase):

    def test_equal(self):
        self.assertTrue(filecmp.cmp('data/SALT.xlsx', '../SALT.xlsx'),
                        'SALT spreadsheet has changed with respect to reference')


if __name__ == '__main__':

    unittest.main()
