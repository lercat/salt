# SALT - Excel

This folder contains the Excel spreadsheet version of the tool that is working with five different aerodynamic loss
categories and a pre-specified spanwise blade discretization. The assumed aerodynamic categorizations that are not finally established,
but will be established in an ongoing project called [LERCat](https://orbit.dtu.dk/en/projects/leading-edge-roughness-categorisation).
The spreadsheet is not locked so make sure you do not change calculations by accident. If so, just download another time.

Based on the publication by e.g [Kruse et al.](https://onlinelibrary.wiley.com/doi/epdf/10.1002/we.2630) the follow preliminary categories were established:

| Category     | Description |
| ----------- | ----------- |
| a   | A new and clean blade blade section with almost no leading edge surface damages or imperfections       |
| b   | A blade with small damages at the leading edge and with a surface roughness smaller than P400 |
| c   | A leading edge with a surface corresponding to sandpaper P400 |
| d   | A leading edge with a surface corresponding to sandpaper P40 or with a cavity 0.3%c deep |
| e   | A leading edge with surface damages more severe than sandpaper P40 or somewhat deeper than 0.3%c  |

They are alphabetic to avoid confusion with structural categorizations, which are usually numeric.

### Contact

The spreadsheet was authored by [Christian Bak](https://orbit.dtu.dk/en/persons/christian-bak) (chba@dtu.dk), DTU Wind and Energy Systems

